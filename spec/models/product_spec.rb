require 'rails_helper'

RSpec.describe "Product", type: :model do
  describe "related_products" do
    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:taxon3) { create(:taxon) }
    let!(:product1) { create(:product, taxons: [taxon1]) }
    let!(:product2) { create(:product, taxons: [taxon2]) }
    let!(:product3) { create(:product, taxons: [taxon2, taxon3]) }
    let!(:product4) { create(:product, taxons: [taxon2, taxon3]) }

    context "There is not related_product (product1)" do
      it { expect(product1.related_products.length).to eq 0 }
    end

    context "product2 has 1 taxon and has related_products (product2)" do
      it { expect(product2.related_products).to match_array([product3, product4]) }
    end

    context "product3 has 2 taxons and has related_products  (product3)" do
      it { expect(product3.related_products).to match_array([product2, product4]) }
    end
  end
end
