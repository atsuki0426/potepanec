require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :request do
  describe "product page" do
    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:taxon3) { create(:taxon) }
    let(:product) { create(:product_with_images, taxons: [taxon1, taxon2]) }
    let!(:related_product1) { create(:product_with_images, taxons: [taxon1], price: 11.11) }
    let!(:related_product2) { create(:product_with_images, taxons: [taxon3], price: 22.22) }

    before do
      get potepan_product_path(product.id)
    end

    it "is successful" do
      expect(response).to have_http_status(:success)
    end

    it "include product.name" do
      expect(response.body).to include product.name
    end

    it "include product.price" do
      expect(response.body).to include product.price.to_money.format
    end

    it "include only product.description" do
      expect(response.body).to include product.description
    end

    it "include related_product" do
      expect(response.body).to include related_product1.name
      expect(response.body).to include related_product1.images.first.attachment(:large)
      expect(response.body).to include related_product1.display_price.to_s
    end

    it "not include related_product2" do
      expect(response.body).not_to include related_product2.name
      expect(response.body).not_to include related_product2.images.first.attachment(:large)
      expect(response.body).not_to include related_product2.display_price.to_s
    end

    context "category is not exist" do
      it "is error" do
        expect { get potepan_product_url 20 } .to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
