require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::CategoriesController, type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon1) { create(:taxon, name: "taxon1", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:taxon2) { create(:taxon, name: "taxon2", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product) { create(:product_with_images, price: 11.11, taxons: [taxon1]) }
  let!(:other_product) { create(:product_with_images, price: 22.22, taxons: [taxon2]) }

  describe "GET potepan/categories#show" do
    describe "category exists" do
      before do
        get potepan_category_path(taxon1.id)
      end

      it "is successful" do
        expect(response).to have_http_status(:success)
      end

      it "include only product.name" do
        expect(response.body).to include product.name
        expect(response.body).not_to include other_product.name
      end

      it "include only product.images" do
        expect(response.body).to include product.images.first.attachment(:large)
        expect(response.body).not_to include other_product.images.first.attachment(:large)
      end

      it "include only product price" do
        expect(response.body).to include product.price.to_money.format
        expect(response.body).not_to include other_product.price.to_money.format
      end

      it "include taxonomy at category_bar" do
        expect(response.body).to include taxonomy.name
      end

      it "include taxon at category_sidebar" do
        expect(response.body).to include taxon1.name
        expect(response.body).to include taxon2.name
      end
    end

    describe "category does not exist" do
      it "is error" do
        expect { get potepan_category_url 20 } .to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
