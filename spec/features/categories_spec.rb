require 'rails_helper'

RSpec.describe 'Categories', type: :feature do
  include ApplicationHelper
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:taxon)    { create(:taxon, name: "taxon", taxonomy: taxonomy, parent_id: taxonomy.root) }
  let!(:taxon2) { create(:taxon, name: "taxon2", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:taxonomy) { create(:taxonomy) }

  feature "potepan/category#show" do
    before do
      visit potepan_category_path(taxon.id)
    end

    scenario "Page title is '(taxon.name) - BIGBAG Store'" do
      expect(page).to have_title(full_title(page_title: taxon.name))
    end

    scenario "goes to home with click_on header_logo" do
      within(".navbar-header") { find('.navbar-brand').click }
      expect(current_path).to eq potepan_path
    end

    scenario "goes to home with click_on #nav_bar_to_home" do
      within(".navbar") { click_link "Home" }
      expect(current_path).to eq potepan_path
    end

    scenario "goes to home with click_on #top_bar_to_home" do
      within(".pageHeader") { click_link "Home" }
      expect(current_path).to eq potepan_path
    end

    scenario "PageHeader has shop link" do
      within ".pageHeader" do
        expect(page).to have_link "shop"
      end
    end

    scenario "goes to product_show_page" do
      within(".productsrow") { find(".product-#{product.id}").click }
      expect(current_path).to eq potepan_product_path(product.id)
    end

    scenario "goes to other_category_page" do
      within(".side-nav") do
        expect(page).to have_content taxonomy.name
        find(".dropdown-toggle").click
        expect(page).to have_link "taxon2"
        click_on "taxon2"
        expect(current_path).to eq potepan_category_path(taxon2.id)
      end
    end
  end
end
