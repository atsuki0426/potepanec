require 'rails_helper'

RSpec.describe "Products", type: :feature do
  include ApplicationHelper
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root) }
  let(:product) { create(:product_with_images, taxons: [taxon]) }
  let!(:related_product) { create(:product_with_images, taxons: [taxon]) }

  feature "potepan/product#show" do
    before do
      visit potepan_product_path(product.id)
    end

    scenario "goes to home with click_on header_logo" do
      within(".navbar-header") { find('.navbar-brand').click }
      expect(current_path).to eq potepan_path
    end

    scenario "goes to home with click_on #nav_bar_to_home" do
      within(".navbar") { click_link "Home" }
      expect(current_path).to eq potepan_path
    end

    scenario "goes to home with click_on #top_bar_to_home" do
      within(".pageHeader") { click_link "Home" }
      expect(current_path).to eq potepan_path
    end

    scenario "pageheader dosent have shop_link" do
      within ".pageHeader" do
        expect(page).not_to have_link "shop"
      end
    end

    scenario "goes to category_page" do
      within(".media-body") { click_link "一覧ページへ戻る" }
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    scenario "goes to related_product_page" do
      within(".productBox") { click_on related_product.name }
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end

  feature "Related_products" do
    context "related_products has 5 products" do
      let!(:products) { create_list(:product_with_images, 5, taxons: [taxon]) }

      before do
        visit potepan_product_path(product.id)
      end

      scenario "#{RELATED_PRODUCTS_MAX_NUM} products are displayed " do
        related_product_count = within(".productsContent") { all(".productBox") }
        expect(related_product_count.length).to eq RELATED_PRODUCTS_MAX_NUM
      end
    end
  end
end
