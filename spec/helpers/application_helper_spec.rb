require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  describe "full_title method" do
    subject { full_title(page_title: title) }

    context "argument is blank" do
      context "no argument" do
        it { expect(full_title).to eq(STORE_NAME) }
      end

      context "argument is nil" do
        let(:title) { nil }

        it { is_expected.to eq(STORE_NAME) }
      end

      context "argument is empty" do
        let(:title) { "" }

        it { is_expected.to eq(STORE_NAME) }
      end
    end

    describe "argument is not blank" do
      context "argument is string" do
        let(:title) { "potepan" }

        it { is_expected.to eq("potepan - #{STORE_NAME}") }
      end

      context "argument is numerical value" do
        let(:title) { 1234 }

        it { is_expected.to eq("1234 - #{STORE_NAME}") }
      end
    end
  end
end
