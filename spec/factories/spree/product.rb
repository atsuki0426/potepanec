FactoryBot.define do
  factory :product_with_images, parent: :product do
    after(:create) do |product|
      product.images << FactoryBot.create(:image)
    end
  end
end
