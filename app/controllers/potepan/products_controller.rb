class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @taxon_id = @product.taxons.first.id
    @related_products = @product.related_products.includes(master: [:images, :default_price]).limit(RELATED_PRODUCTS_MAX_NUM)
  end
end
